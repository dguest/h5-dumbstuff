#!/usr/bin/env python3

from h5py import File
from numpy.lib.recfunctions import structured_to_unstructured
import numpy as np

def run():
    f = File('my-lovely.h5')
    g = f['caloCells']
    event = g['1d']
    jets = g['2d']
    # this will give you a sub-array of a selection, it has keys still
    thebomb =  jets.fields(['AntiKt4EMTopoJets_AverageLArQF',
                           'AntiKt4EMTopoJets_E'])
    print('thebomb: ', thebomb[1,1].dtype, thebomb[1:5, 3])

    # you can also access the keys like this
    one_field = thebomb['AntiKt4EMTopoJets_AverageLArQF'][1,3]
    print('one entry: ', one_field.dtype, one_field)

    # this will give you a simpler array
    e = jets.fields('AntiKt4EMTopoJets_E')
    print('one field: ', e[1].dtype, e[1:5, 3])

    # you can convert to numpy structured array
    np_arr = np.array(thebomb)
    print('as np: ', np_arr.dtype, np_arr[1:5, 3])
    # you can also flatten into a unstructured array
    flat_arr = structured_to_unstructured(np_arr, dtype=float)
    # note the three indices now, the 1th one is E again
    print('flattened: ', flat_arr[1:5, 3, 1])

if __name__ == '__main__':
    run()
